# fipe-depreciacao-app

### Descrição

App para calculo de depreciação de veículos ao longo do tempo de acordo com a tabela FIPE, divido em duas partes:

* Server(fipe-depreciacao-api): Restful service consumindo a API publica http://fipeapi.appspot.com/ e expondo serviços ao frontend.
    * Tecnologias utilizadas:
        * Spring Boot
        * Spring Web
        * JPA
        * Spring-data-mongodb
        * Lombok
        * Junit
        * Mockito

* Client(fipe-depreciacao-ui): Frontend com apenas uma tela, consumindo serviços expostos pelo backend, permitindo ao usuário selecionar marca e veículo que desejar calcular depreciação.
    * Tecnologias utilizadas:
        * Angular 8
        * Angular Material

### Requerimentos

* Docker
* Docker-compose

### Execução

Para executar a aplicação, basta ir na raiz do projeto e executar o comando abaixo. Todas as dependências necessárias serão baixadas e aplicação será inicializada na sequência.


````sh
docker-compose up
````

### Teste

Client acessível localmente no contexto raiz da porta 4200 e server no contexto raiz da porta 8080.

* Client: http://localhost:4200/
* Server: http://localhost:8080/