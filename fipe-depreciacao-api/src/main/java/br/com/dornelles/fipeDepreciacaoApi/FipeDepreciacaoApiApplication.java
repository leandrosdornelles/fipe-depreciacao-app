package br.com.dornelles.fipeDepreciacaoApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FipeDepreciacaoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FipeDepreciacaoApiApplication.class, args);
	}

}
