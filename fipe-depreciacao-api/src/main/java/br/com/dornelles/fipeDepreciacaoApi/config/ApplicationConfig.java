package br.com.dornelles.fipeDepreciacaoApi.config;

import br.com.dornelles.fipeDepreciacaoApi.controller.UserRequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@EnableWebMvc
@Configuration
@ComponentScan
public class ApplicationConfig implements WebMvcConfigurer {

    public static final String FIPE_API_ROOT_PATH = "http://fipeapi.appspot.com/api/1/";

    @Autowired
    UserRequestInterceptor userRequestInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(this.userRequestInterceptor);
    }
}
