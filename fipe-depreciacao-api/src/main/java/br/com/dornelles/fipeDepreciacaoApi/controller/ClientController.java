package br.com.dornelles.fipeDepreciacaoApi.controller;

import br.com.dornelles.fipeDepreciacaoApi.model.*;
import br.com.dornelles.fipeDepreciacaoApi.service.FipeApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/api/v1")
public class ClientController {

    private final FipeApiService fipeApiService;

    @CrossOrigin
    @GetMapping("/marcas")
    public ResponseEntity<List<MarcaDto>> getMarcas() {
        log.debug("Finding marcas");
        List<MarcaDto> marcaDtoList = fipeApiService.getMarcas();
        log.debug("Found marcas sucessfully");
        return new ResponseEntity<>(marcaDtoList, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("{idMarca}/veiculos")
    public ResponseEntity<List<VeiculoDto>> getVeiculosByMarca(@PathVariable("idMarca") Integer idMarca) {
        log.debug("Finding veiculos by marca id: " + idMarca);
        List<VeiculoDto> veiculoDtoList = fipeApiService.getListVeiculo(idMarca);
        log.debug("Found veiculos sucessfully");
        return new ResponseEntity<>(veiculoDtoList, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("{idMarca}/{idVeiculo}/veiculosAno")
    public ResponseEntity<List<VeiculoAnoDto>> getVeiculosAno(@PathVariable("idMarca") Integer idMarca, @PathVariable("idVeiculo") Integer idVeiculo) {
        log.debug("Finding veiculos Ano by marca id: " + idMarca + " and veiculo id: " + idVeiculo);
        List<VeiculoAnoDto> veiculoAnoDtoList = fipeApiService.getListVeiculoAno(idMarca, idVeiculo);
        log.debug("Found veiculos ano sucessfully");
        return new ResponseEntity<>(veiculoAnoDtoList, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("{idMarca}/{idVeiculo}/{AnoSemestre}")
    public ResponseEntity<VeiculoAnoDetalheDto> getVeiculoDetalhe(@PathVariable("idMarca") Integer idMarca, @PathVariable("idVeiculo") Integer idVeiculo, @PathVariable("AnoSemestre") AnoSemestreVo AnoSemestre) {
        log.debug("Finding veiculo detalhe by marca id: " + idMarca + " and veiculo id: " + idVeiculo + " and anoSemestre: " + AnoSemestre);
        VeiculoAnoDetalheDto veiculoAnoDetalheDto = fipeApiService.getVeiculoDetalhe(idMarca, idVeiculo, AnoSemestre.getRawValue());
        log.debug("Found veiculo detalhe sucessfully");
        return new ResponseEntity<>(veiculoAnoDetalheDto, HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping("{idMarca}/{idVeiculo}/deterioracao")
    public ResponseEntity<VeiculoDeterioracaoDto> getVeiculosDeterioracao(@PathVariable("idMarca") Integer idMarca, @PathVariable("idVeiculo") Integer idVeiculo) {
        log.debug("Finding veiculo deterioracao by marca id: " + idMarca + " and veiculo id: " + idVeiculo);
        VeiculoDeterioracaoDto veiculoDeterioracaoDto = fipeApiService.getVeiculoDeterioracao(idMarca, idVeiculo);
        log.debug("Found veiculo deterioracao sucessfully");
        return new ResponseEntity<>(veiculoDeterioracaoDto, HttpStatus.OK);
    }
}
