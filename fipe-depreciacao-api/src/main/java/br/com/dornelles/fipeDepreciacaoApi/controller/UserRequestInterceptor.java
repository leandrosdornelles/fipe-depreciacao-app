package br.com.dornelles.fipeDepreciacaoApi.controller;

import br.com.dornelles.fipeDepreciacaoApi.service.UserRequestFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;

@Component
public class UserRequestInterceptor extends HandlerInterceptorAdapter {

    private UserRequestFacade userRequestFacade;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        LocalDateTime inicioRequest = LocalDateTime.now();
        request.setAttribute("inicioRequest", inicioRequest);
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        LocalDateTime inicioRequest = (LocalDateTime) request.getAttribute("inicioRequest");
        LocalDateTime finalRequest = LocalDateTime.now();
        this.userRequestFacade.add(inicioRequest, finalRequest, request.getRequestURI(), response.getStatus());
    }

    @Autowired
    public void setUserRequestFacade(UserRequestFacade userRequestFacade) {
        this.userRequestFacade = userRequestFacade;
    }
}
