package br.com.dornelles.fipeDepreciacaoApi.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Document
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRequest {

    @Id
    private ObjectId _id;
    private LocalDateTime inicioRequest;
    private LocalDateTime finalRequest;
    private String requestUri;
    private Integer responseStatus;

    public UserRequest(LocalDateTime inicioRequest, LocalDateTime finalRequest, String requestUri, Integer responseStatus) {
        this.inicioRequest = inicioRequest;
        this.finalRequest = finalRequest;
        this.requestUri = requestUri;
        this.responseStatus = responseStatus;
    }
}
