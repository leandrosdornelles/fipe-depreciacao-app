package br.com.dornelles.fipeDepreciacaoApi.model;

import java.util.Arrays;

public class AnoSemestreVo {
    static final String SEPARADOR_TRACO = "-";

    String rawValue;
    Integer ano;
    Integer semestre;

    public AnoSemestreVo(String anoSemestreKey) {
        if (!anoSemestreKey.contains(SEPARADOR_TRACO)) {
            throw new IllegalArgumentException("Ano semestre key invalido");
        }
        rawValue = anoSemestreKey;
        int[] anoSemestreKeySplit = Arrays.stream(anoSemestreKey.split("-")).mapToInt(s -> Integer.parseInt(s)).toArray();
        ano = anoSemestreKeySplit[0];
        semestre = anoSemestreKeySplit[1];
    }

    public Integer getAno() {
        return ano;
    }

    public Integer getSemestre() {
        return semestre;
    }

    public String getRawValue() {
        return rawValue;
    }
}
