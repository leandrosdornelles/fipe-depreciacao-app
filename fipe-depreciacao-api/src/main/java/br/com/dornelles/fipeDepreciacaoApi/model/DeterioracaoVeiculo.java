package br.com.dornelles.fipeDepreciacaoApi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class DeterioracaoVeiculo {

    private Integer ano;
    private BigDecimal valor;
    private BigDecimal valorDeterioracao;
    private Double percentualDeterioracao;

    public DeterioracaoVeiculo(Integer ano, BigDecimal valor, DeterioracaoVeiculo deterioracaoVeiculoAnterior) {
        this.ano = ano;
        this.valor = valor;
        this.valorDeterioracao = deterioracaoVeiculoAnterior.getValor().compareTo(BigDecimal.ZERO) == 1 ? deterioracaoVeiculoAnterior.getValor().subtract(valor) : BigDecimal.ZERO;
        this.percentualDeterioracao = valorDeterioracao.compareTo(BigDecimal.ZERO) == 1 ? valorDeterioracao.multiply(BigDecimal.valueOf(100)).divide(deterioracaoVeiculoAnterior.getValor(), 0, RoundingMode.DOWN).doubleValue() : 0.0;
    }
}
