package br.com.dornelles.fipeDepreciacaoApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarcaDto {

    @JsonProperty("key")
    private String key;
    @JsonProperty(value = "id", required = true)
    private Integer id;
    @JsonProperty("fipe_name")
    private String fipeName;
    @JsonProperty(value = "name", required = true)
    private String name;
}
