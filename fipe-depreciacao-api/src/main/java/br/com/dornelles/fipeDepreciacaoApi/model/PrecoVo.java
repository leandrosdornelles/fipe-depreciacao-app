package br.com.dornelles.fipeDepreciacaoApi.model;

import org.springframework.util.Assert;

import java.math.BigDecimal;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PrecoVo {

    String rawValue;
    String currency;
    BigDecimal value;

    public PrecoVo(String preco) {
        Assert.notNull(preco, "Variavel preco do veiculo nao informado");
        rawValue = preco;
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(preco);
        int inicioValor = m.find() ? m.start() : -1;
        Assert.isTrue(inicioValor > 0, "Valor do veiculo nao informado");

        currency = preco.substring(0, inicioValor).trim();
        value = new BigDecimal(preco.substring(inicioValor, preco.length()).replace(".", "").replace(",", "."));
    }

    public String getRawValue() {
        return rawValue;
    }

    public String getCurrency() {
        return currency;
    }

    public BigDecimal getValue() {
        return value;
    }
}
