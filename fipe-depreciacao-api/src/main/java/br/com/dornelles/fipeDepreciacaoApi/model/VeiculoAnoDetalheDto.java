package br.com.dornelles.fipeDepreciacaoApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VeiculoAnoDetalheDto {

    @JsonProperty(value = "id", required = true)
    private Integer id;
    @JsonProperty("ano_modelo")
    private Integer anoModelo;
    @JsonProperty("marca")
    private String marca;
    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty("veiculo")
    private String veiculo;
    @JsonProperty("preco")
    private PrecoVo preco;
    @JsonProperty("combustivel")
    private String combustivel;
    @JsonProperty("referencia")
    private String referencia;
    @JsonProperty("fipe_codigo")
    private String fipeCodigo;
    @JsonProperty("key")
    private String key;
}
