package br.com.dornelles.fipeDepreciacaoApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VeiculoAnoDto {

    @JsonProperty("fipe_codigo")
    private AnoSemestreVo fipeCodigo;
    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty("key")
    private AnoSemestreVo key;
    @JsonProperty("veiculo")
    private String veiculo;
    @JsonProperty(value = "id", required = true)
    private AnoSemestreVo id;

}
