package br.com.dornelles.fipeDepreciacaoApi.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class VeiculoDeterioracaoDto {

    private Integer idMarca;
    private Integer idVeiculo;
    private List<DeterioracaoVeiculo> deterioracaoVeiculoList;

    public VeiculoDeterioracaoDto(Integer idMarca, Integer idVeiculo) {
        this.idMarca = idMarca;
        this.idVeiculo = idVeiculo;
        deterioracaoVeiculoList = new ArrayList<>();
    }

    public void addDeterioracaoAno(Integer ano, BigDecimal valor) {
        DeterioracaoVeiculo deterioracaoVeiculo = deterioracaoVeiculoList.size() > 0 ? new DeterioracaoVeiculo(ano, valor, deterioracaoVeiculoList.get(deterioracaoVeiculoList.size() - 1)) : new DeterioracaoVeiculo(ano, valor, BigDecimal.ZERO, 0.0);
        deterioracaoVeiculoList.add(deterioracaoVeiculo);
    }
}
