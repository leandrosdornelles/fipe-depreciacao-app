package br.com.dornelles.fipeDepreciacaoApi.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VeiculoDto {

    @JsonProperty("key")
    private String key;
    @JsonProperty(value = "name", required = true)
    private String name;
    @JsonProperty(value = "id", required = true)
    private Integer id;
    @JsonProperty("fipe_name")
    private String fipeName;

}
