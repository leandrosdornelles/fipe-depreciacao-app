package br.com.dornelles.fipeDepreciacaoApi.repository;

import br.com.dornelles.fipeDepreciacaoApi.domain.UserRequest;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRequestRepository extends MongoRepository<UserRequest, String> {
}