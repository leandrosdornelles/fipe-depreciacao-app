package br.com.dornelles.fipeDepreciacaoApi.service;

import br.com.dornelles.fipeDepreciacaoApi.model.*;

import java.util.List;

public interface FipeApiService {
    List<MarcaDto> getMarcas();

    List<VeiculoDto> getListVeiculo(Integer idMarca);

    List<VeiculoAnoDto> getListVeiculoAno(Integer idMarca, Integer idVeiculo);

    VeiculoAnoDetalheDto getVeiculoDetalhe(Integer idMarca, Integer idVeiculo, String idVeiculoAno);

    VeiculoDeterioracaoDto getVeiculoDeterioracao(Integer idMarca, Integer idVeiculo);

}
