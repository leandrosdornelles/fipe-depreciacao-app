package br.com.dornelles.fipeDepreciacaoApi.service;

import br.com.dornelles.fipeDepreciacaoApi.model.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import static br.com.dornelles.fipeDepreciacaoApi.config.ApplicationConfig.FIPE_API_ROOT_PATH;

import java.util.List;

@Slf4j
@Service
public class FipeApiServiceRestTemplateImpl implements FipeApiService {

    private final RestTemplate restTemplate;

    public FipeApiServiceRestTemplateImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public List<MarcaDto> getMarcas() {
        ResponseEntity<List<MarcaDto>> response = restTemplate.exchange(FIPE_API_ROOT_PATH + "carros/marcas.json",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<MarcaDto>>() {
                });
        return response.getBody();
    }

    @Override
    public List<VeiculoDto> getListVeiculo(Integer idMarca) {
        ResponseEntity<List<VeiculoDto>> response = restTemplate.exchange(FIPE_API_ROOT_PATH + "carros/veiculos/" + idMarca + ".json",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<VeiculoDto>>() {
                });
        return response.getBody();
    }

    @Override
    public List<VeiculoAnoDto> getListVeiculoAno(Integer idMarca, Integer idVeiculo) {
        ResponseEntity<List<VeiculoAnoDto>> response = restTemplate.exchange(FIPE_API_ROOT_PATH + "carros/veiculo/" + idMarca + "/" + idVeiculo + ".json",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<VeiculoAnoDto>>() {
                });
        return response.getBody();
    }

    @Override
    public VeiculoAnoDetalheDto getVeiculoDetalhe(Integer idMarca, Integer idVeiculo, String idVeiculoAno) {
        return restTemplate.getForObject(FIPE_API_ROOT_PATH + "carros/veiculo/" + idMarca + "/" + idVeiculo + "/" + idVeiculoAno + ".json", VeiculoAnoDetalheDto.class);
    }

    @Override
    public VeiculoDeterioracaoDto getVeiculoDeterioracao(Integer idMarca, Integer idVeiculo) {
        VeiculoDeterioracaoDto veiculoDeterioracaoDto = new VeiculoDeterioracaoDto(idMarca, idVeiculo);
        this.getListVeiculoAno(idMarca, idVeiculo).stream()
                .map(v -> this.getVeiculoDetalhe(idMarca, idVeiculo, v.getId().getRawValue()))
                .forEach(v -> veiculoDeterioracaoDto.addDeterioracaoAno(v.getAnoModelo(), v.getPreco().getValue()));

        return veiculoDeterioracaoDto;
    }


}
