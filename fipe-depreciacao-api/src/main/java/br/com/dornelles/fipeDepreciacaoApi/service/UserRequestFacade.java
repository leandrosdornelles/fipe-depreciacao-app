package br.com.dornelles.fipeDepreciacaoApi.service;

import br.com.dornelles.fipeDepreciacaoApi.domain.UserRequest;
import br.com.dornelles.fipeDepreciacaoApi.repository.UserRequestRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserRequestFacade {

    private UserRequestRepository userRequestRepository;

    @Autowired
    public UserRequestFacade(UserRequestRepository userRequestRepository) {
        this.userRequestRepository = userRequestRepository;
    }

    public void add(LocalDateTime inicioRequest, LocalDateTime finalRequest, String requestUri, Integer httpStatus) {
        UserRequest userRequest = new UserRequest(inicioRequest, finalRequest, requestUri, httpStatus);
        this.userRequestRepository.save(userRequest);
    }
}
