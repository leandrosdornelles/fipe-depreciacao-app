package br.com.dornelles.fipeDepreciacaoApi.controller;

import br.com.dornelles.fipeDepreciacaoApi.model.VeiculoDeterioracaoDto;
import br.com.dornelles.fipeDepreciacaoApi.service.FipeApiService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class ClientControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FipeApiService fipeApiService;

    @Test
    public void getVeiculosDeterioracao() throws Exception{
        VeiculoDeterioracaoDto veiculoDeterioracaoDto = new VeiculoDeterioracaoDto(1, 2);
        veiculoDeterioracaoDto.addDeterioracaoAno(2020, new BigDecimal(50000));
        veiculoDeterioracaoDto.addDeterioracaoAno(2019, new BigDecimal(45000));
        when(fipeApiService.getVeiculoDeterioracao(1, 2)).thenReturn(veiculoDeterioracaoDto);

        this.mockMvc.perform(get("/api/v1/1/2/deterioracao")).
                andExpect(status().isOk()).
                andExpect(content().json("{\"idMarca\":1,\"idVeiculo\":2,\"deterioracaoVeiculoList\":[{\"ano\":2020,\"valor\":50000,\"valorDeterioracao\":0,\"percentualDeterioracao\":0},{\"ano\":2019,\"valor\":45000,\"valorDeterioracao\":5000,\"percentualDeterioracao\":10}]}"));

    }
}