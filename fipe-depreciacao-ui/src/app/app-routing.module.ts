import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepreciacaoComponent } from './depreciacao/depreciacao.component';


const routes: Routes = [
{path:'', component: DepreciacaoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
