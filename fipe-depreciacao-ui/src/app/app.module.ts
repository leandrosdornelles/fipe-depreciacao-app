import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import {CurrencyPipe, registerLocaleData, CommonModule} from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DepreciacaoComponent } from './depreciacao/depreciacao.component';
import { HttpClientModule } from '@angular/common/http';

import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDialogModule } from '@angular/material/dialog';
import {MatToolbarModule} from '@angular/material/toolbar';
import { FormsModule } from '@angular/forms';
import {MatSelectModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'; 
import localebr from '@angular/common/locales/br';

registerLocaleData(localebr, 'br');

@NgModule({
  declarations: [
    AppComponent,
    DepreciacaoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  HttpClientModule,
  BrowserAnimationsModule,
  FormsModule,
  CommonModule,
  MatToolbarModule,
	MatDialogModule, MatButtonModule, MatCardModule, MatSelectModule, MatInputModule, MatFormFieldModule
  ],
  providers: [CurrencyPipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
