import { Component, OnInit, ViewChild, ElementRef  } from '@angular/core';
import { HttpClientService, Marca, Veiculo, DeterioracaoValor, VeiculoDeterioracao } from '../service/http-client.service';
import {CurrencyPipe } from '@angular/common';
import { tap, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-depreciacao',
  templateUrl: './depreciacao.component.html',
  styleUrls: ['./depreciacao.component.css']  
})
export class DepreciacaoComponent implements OnInit {

  @ViewChild('dataContainer', {static: false}) dataContainer: ElementRef;
	
  marcas:Marca[];
  veiculos:Veiculo[];
  veiculoDeterioracao:VeiculoDeterioracao;
  loading:boolean = false;
  cp;

  constructor(
    private httpClientService:HttpClientService,
    public cpa:CurrencyPipe
  ) {this.cp= cpa; }

  getVeiculos(id: number)
  {
    this.httpClientService.getVeiculos(id).subscribe(
      response => this.veiculos = response
     );
  }

  getVeiculoDeterioracao(idMarca: number, idVeiculo: number){    
    this.httpClientService.getVeiculoDepreciacao(idMarca, idVeiculo).subscribe(
      response => { this.veiculoDeterioracao = response;
                    let valor = "";
                    let deterioracaoVeiculoLocal = this.veiculoDeterioracao.deterioracaoVeiculoList;
                    deterioracaoVeiculoLocal.forEach(function(element, index){
                      let ano = element.ano === 32000 ? "Zero KM" : element.ano;
                      valor = valor + "- Valor em " + ano + " -> " + this.formatValorEmReais(element.valor);
                      if(index > 0 && element.valorDeterioracao !== 0){
                        let anoAnterior = deterioracaoVeiculoLocal[index - 1].ano === 32000 ? "Zero KM" : deterioracaoVeiculoLocal[index - 1].ano;
                        valor = valor + ". Alteração de " + this.formatValorEmReais(element.valorDeterioracao) + " (" + element.percentualDeterioracao + "%) em relação a " + anoAnterior;
                      }
                      valor = valor + " \n";
                    }.bind(this));
                    this.dataContainer.nativeElement.value = valor;
                  }
    );
  }
  
  ngOnInit() {
    this.httpClientService.getMarcas().subscribe(
     response => this.marcas = response
    );
  }
  
  formatValorEmReais(valor:number){
    return this.cp.transform(valor, 'BRL');
  };
}
