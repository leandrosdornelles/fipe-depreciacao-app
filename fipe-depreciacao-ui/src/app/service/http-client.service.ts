import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Marca{
	constructor(
		public key:string,
		public id:number,
		public fipename:string,
		public name:string
	) {}
}

export class Veiculo{
	constructor(
		public key:string,
		public name:string,
		public id:number,
		public fipename:string		
	) {}
}

export class DeterioracaoValor{
	constructor(
		public ano:number,
		public percentualDeterioracao:number,
		public valor:number,
		public valorDeterioracao:number			
	) {}
}

export class VeiculoDeterioracao{
	constructor(
		public idmarca:number,
		public idveiculo:number,
		public deterioracaoVeiculoList:DeterioracaoValor[]
	){}
}	

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient:HttpClient
  ) { 
     }

     getMarcas()
  {
    console.log("calling getMarcas");
    return this.httpClient.get<Marca[]>('http://localhost:8080/api/v1/marcas');
	//return this.httpClient.get<Marca[]>('http://fipe-depreciacao-api:8080/api/v1/marcas');
  }
  
       getVeiculos(id: number)
  {
    console.log("calling getVeiculos");
    return this.httpClient.get<Veiculo[]>('http://localhost:8080/api/v1/' + id + '/veiculos');
	//return this.httpClient.get<Veiculo[]>('http://localhost:8080/api/v1/' + id + 'veiculos');
  }
  
      getVeiculoDepreciacao(idMarca: number, idVeiculo: number)
  {
    console.log("calling getVeiculoDepreciacao");
    return this.httpClient.get<VeiculoDeterioracao>('http://localhost:8080/api/v1/' + idMarca + '/' + idVeiculo + '/' + "deterioracao");
  }
}
